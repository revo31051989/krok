import 'package:flutter/material.dart';

const _avatarSize = 48.0;

class AchievementItem extends StatelessWidget {
  const AchievementItem({super.key, this.photo, this.name, this.body});

  final String? photo;
  final String? name;
  final String? body;

  @override
  Widget build(BuildContext context) {
    final photo = this.photo;
    return Column(
      children: [
        CircleAvatar(
          radius: _avatarSize,
          backgroundImage: photo != null ? NetworkImage(photo) : null,
          child: photo == null
              ? const Icon(Icons.person_outline, size: _avatarSize)
              : null,
        ),
        Text(name == null ? '' : name!),
        Text(body == null ? '' : body!)
      ],
    );
  }
}

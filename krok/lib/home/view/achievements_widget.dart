import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:krok/home/view/achievements_widget_card.dart';

class Achievements extends StatefulWidget {
  final String email;

  const Achievements(this.email, {super.key});

  @override
  AchievementsState createState() => AchievementsState();
}

class AchievementsState extends State<Achievements> {
  List<Widget> data = [];
  bool choisen = false;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: FirebaseFirestore.instance.collection(widget.email).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            if (!choisen) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return data.isEmpty
                  ? const SizedBox()
                  : Expanded(
                      child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: data.length,
                        shrinkWrap: true,
                        physics: const ClampingScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return data[index];
                        },
                      ),
                    );
            }
          } else {
            choisen = true;
            return snapshot.data == null
                ? const SizedBox()
                : ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: snapshot.data!.docs.length,
                    shrinkWrap: true,
                    physics: const ClampingScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      data.add(AchievementsCard(
                        path: snapshot.data!.docs[index].get('icon'),
                        title: snapshot.data!.docs[index].get('name'),
                        body: snapshot.data!.docs[index].get('body'),
                      ));
                      return const SizedBox();
                    },
                  );
          }
        });
  }
}

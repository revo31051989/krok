import 'package:flutter/material.dart';
import 'package:krok/home/view/home_page.dart';

class AchievementsPage extends StatelessWidget {
  final String path;
  final String title;
  final String body;

  const AchievementsPage(
      {super.key, required this.path, required this.title, required this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: IconButton(
          alignment: Alignment.centerRight,
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pushReplacement(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    const HomePage(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
          },
        ),
      ),
      body: Column(
        children: [
          Image.asset(
            path,
            height: 30,
            width: 30,
          ),
          Text(title),
          Text(body),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:krok/home/view/achievements_page.dart';

class AchievementsCard extends StatelessWidget {
  final String path;
  final String title;
  final String body;

  const AchievementsCard(
      {super.key, required this.path, required this.title, required this.body});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Image.asset(
            path,
            height: 30,
            width: 30,
          ),
          onTap: () {
            Navigator.of(context, rootNavigator: true).pushReplacement(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    AchievementsPage(
                  path: path,
                  title: title,
                  body: body,
                ),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
          },
        ),
      ],
    );
  }
}

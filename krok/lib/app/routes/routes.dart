import 'package:flutter/widgets.dart';
import 'package:krok/app/bloc/app_state.dart';
import 'package:krok/home/home.dart';
import 'package:krok/login/view/login_page.dart';

List<Page<dynamic>> onGenerateAppViewPages(
  AppStatus state,
  List<Page<dynamic>> pages,
) {
  switch (state) {
    case AppStatus.authenticated:
      return [HomePage.page()];
    case AppStatus.unauthenticated:
      return [LoginPage.page()];
  }
}
